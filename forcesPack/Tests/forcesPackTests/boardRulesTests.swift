//
//  File.swift
//  
//
//  Created by yorick geoffre on 07/02/2023.
//

import Foundation
import XCTest
import forcesPack

final class BoardRulesTests: XCTestCase {
    private var boardEmpty: [[Int?]] = [[nil,nil,nil,nil],[nil,nil,nil,nil],[nil,nil,nil,nil],[nil,nil,nil,nil]]
    private var boardRow: [[Int?]] = [[nil,nil,nil,nil],[nil,nil,nil,nil],[0,0,0,0],[nil,nil,nil,nil]]
    private var boardCol: [[Int?]] = [[nil,0,nil,nil],[nil,0,nil,nil],[nil,0,nil,nil],[nil,0,nil,nil],]
    private var boardDiag: [[Int?]] = [[0,nil,nil,nil],[nil,0,nil,nil],[nil,nil,0,nil],[nil,nil,nil,0]]
    private var boardDiagInv: [[Int?]] = [[nil,nil,nil,0],[nil,nil,0,nil],[nil,0,nil,nil],[0,nil,nil,nil]]
    
    private var boardRowBig: [[Int?]] = [[nil,nil,nil,nil],[nil,nil,nil,nil],[nil,nil,nil,nil],[nil,nil,nil,nil],[0,0,0,0],[nil,nil,nil,nil]]
    private var boardColBig: [[Int?]] = [[nil,nil,nil,nil],[nil,nil,nil,nil],[nil,0,nil,nil],[nil,0,nil,nil],[nil,0,nil,nil],[nil,0,nil,nil],]
    private var boardDiagBig: [[Int?]] = [[nil,nil,nil,nil],[nil,nil,nil,nil],[0,nil,nil,nil],[nil,0,nil,nil],[nil,nil,0,nil],[nil,nil,nil,0]]
    private var boardDiagInvBig: [[Int?]] = [[nil,nil,nil,nil],[nil,nil,nil,nil],[nil,nil,nil,0],[nil,nil,0,nil],[nil,0,nil,nil],[0,nil,nil,nil]]
    
    func testRules(){
        var ruleCol : Rule = AreCols4x()
        var ruleRow: Rule = AreRows4x()
        var ruleDiag:  Rule = AreDiags4x()
        var ruleRowL: Rule = AreRows4xL()
        var ruleColL: Rule = AreCols4xL()
        var ruleDiagL: Rule = AreDiags4xL()
        
        XCTAssertFalse(ruleCol.isRuleMet(grid: boardEmpty, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertFalse(ruleRow.isRuleMet(grid: boardEmpty, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertFalse(ruleDiag.isRuleMet(grid: boardEmpty, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        XCTAssertTrue(ruleCol.isRuleMet(grid: boardRow, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertFalse(ruleRow.isRuleMet(grid: boardRow, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertFalse(ruleDiag.isRuleMet(grid: boardRow, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        print(ruleCol.results)
        
        XCTAssertFalse(ruleCol.isRuleMet(grid: boardCol, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertTrue(ruleRow.isRuleMet(grid: boardCol, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertFalse(ruleDiag.isRuleMet(grid: boardCol, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        print(ruleRow.results)
        
        XCTAssertFalse(ruleCol.isRuleMet(grid: boardDiag, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertFalse(ruleRow.isRuleMet(grid: boardDiag, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        XCTAssertTrue(ruleDiag.isRuleMet(grid: boardDiag, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        
        XCTAssertTrue(ruleColL.isRuleMet(grid: boardRow, targetedRow: 2, targetedCol: 2, lastPlayedId: 0))
        XCTAssertFalse(ruleRowL.isRuleMet(grid: boardRow, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertFalse(ruleDiagL.isRuleMet(grid: boardRow, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        XCTAssertFalse(ruleColL.isRuleMet(grid: boardCol, targetedRow: 2, targetedCol: 2, lastPlayedId: 0))
        XCTAssertTrue(ruleRowL.isRuleMet(grid: boardCol, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertFalse(ruleDiagL.isRuleMet(grid: boardCol, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        XCTAssertFalse(ruleColL.isRuleMet(grid: boardDiag, targetedRow: 2, targetedCol: 2, lastPlayedId: 0))
        XCTAssertFalse(ruleRowL.isRuleMet(grid: boardDiag, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertTrue(ruleDiagL.isRuleMet(grid: boardDiag, targetedRow: 0, targetedCol: 0, lastPlayedId: 0))
        
        XCTAssertFalse(ruleColL.isRuleMet(grid: boardDiagInv, targetedRow: 2, targetedCol: 2, lastPlayedId: 0))
        XCTAssertFalse(ruleRowL.isRuleMet(grid: boardDiagInv, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertTrue(ruleDiagL.isRuleMet(grid: boardDiagInv, targetedRow: 0, targetedCol: 3, lastPlayedId: 0))
        
        print(ruleRowL.results)
        print(ruleColL.results)
        print(ruleDiagL.results)
        
        XCTAssertTrue(ruleColL.isRuleMet(grid: boardRowBig, targetedRow: 4, targetedCol: 4, lastPlayedId: 0))
        XCTAssertFalse(ruleRowL.isRuleMet(grid: boardRowBig, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertFalse(ruleDiagL.isRuleMet(grid: boardRowBig, targetedRow: 2, targetedCol: 4, lastPlayedId: 0))
        
        XCTAssertFalse(ruleColL.isRuleMet(grid: boardColBig, targetedRow: 4, targetedCol: 4, lastPlayedId: 0))
        XCTAssertTrue(ruleRowL.isRuleMet(grid: boardColBig, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertFalse(ruleDiagL.isRuleMet(grid: boardColBig, targetedRow: 2, targetedCol: 4, lastPlayedId: 0))
        
        XCTAssertFalse(ruleColL.isRuleMet(grid: boardDiagBig, targetedRow: 4, targetedCol: 4, lastPlayedId: 0))
        XCTAssertFalse(ruleRowL.isRuleMet(grid: boardDiagBig, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertTrue(ruleDiagL.isRuleMet(grid: boardDiagBig, targetedRow: 2, targetedCol: 0, lastPlayedId: 0))
        
        XCTAssertFalse(ruleColL.isRuleMet(grid: boardDiagInvBig, targetedRow: 4, targetedCol: 4, lastPlayedId: 0))
        XCTAssertFalse(ruleRowL.isRuleMet(grid: boardDiagInvBig, targetedRow: 1, targetedCol: 1, lastPlayedId: 0))
        XCTAssertTrue(ruleDiagL.isRuleMet(grid: boardDiagInvBig, targetedRow: 2, targetedCol: 3, lastPlayedId: 0))
    }
}
