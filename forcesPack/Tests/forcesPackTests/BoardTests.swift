import XCTest
@testable import forcesPack

final class _forcesPackTests: XCTestCase {
    func testBoard() throws {
        var b: Board = Board()
        XCTAssertFalse(b.isFull())
        XCTAssertEqual(-404, b[-1,-1])
        XCTAssertEqual(-404, b[9999,9999])
        XCTAssertNil(b[0,0])
        for col in 0...3{
            for n in 0...3{
                print("----------------------------\niteration", col*4+n)
                XCTAssertTrue(b.insertPeice(id: 9, row: col))
            }
        }
        
        XCTAssertTrue(b.isFull())
        print(b)
        
        XCTAssertNotNil(b[0,0] == 9)
        
        let b2: Board = Board(nbRows: 10, nbCol: 10)!
        XCTAssertFalse(b2.isFull())
        
        b = Board()
        XCTAssertFalse(b.insertPeice(id: 0, row: -1))
        
        let i: [[Int?]] = Array(repeating: Array( repeating: 0, count: 3), count: 3)
        
        var bz: Board? = Board(input: i)
        XCTAssertNotNil(bz)
    }
}
