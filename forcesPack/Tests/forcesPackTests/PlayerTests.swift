//
//  PlayerTests.swift
//  
//
//  Created by yorick geoffre on 26/01/2023.
//

import XCTest
import forcesPack

final class PlayerTests: XCTestCase {
    func reader(_ ign: Bool) -> String?{
        return ""
    }
    
    func testPlayers(){
        var p: Player = IA(id: 0, reader: reader)
        XCTAssertNotNil(p)
        var ret = p.play(10)
        XCTAssertTrue( ret <= 10 && ret >= 0)
        XCTAssertEqual(p.description, "IA 0")
        
        p = Player(id: 0, reader: reader)
        XCTAssertNotNil(p)
        ret = p.play(10)
        XCTAssertTrue( ret == -1)
        XCTAssertEqual(p.description, "Player 0")
        
        p = Human(id: 0, reader: reader)
        XCTAssertNotNil(p)
        XCTAssertEqual(p.description, "Human 0")
    }
}
