//
//  Game.swift
//  4forces
//
//  Created by yorick geoffre on 17/01/2023.
//

import Foundation

public class Game{
    private var board: Board
    private var players: [Player]
    private var winRules: [Rule]
    private var winner: Player?
    private var printerFunc: (String)->()
    private var readerFunc: (Bool)-> String?
    
    private var rows: Int = 6
    private var cols: Int = 7
    
    public init(printer: @escaping (String)->(),reader: @escaping (Bool)->String?) {
        readerFunc = reader
        printerFunc = printer
        board = Board(nbRows: rows,nbCol: cols)!
        players = []
        winRules = [AreCols4xL(), AreRows4xL(), AreDiags4xL()]
        createBoard()
        board = Board(nbRows: rows,nbCol: cols)!
        while(!CreatePlayers()){}
        
        while(true){
            while(doGameLoop()){}
            printerFunc(winner == nil ? "stalemate!" : "player: "+String(winner?.id ?? -1)+" has won the game")
            if(winner != nil){ break }
            sleep(1)
            winner = nil
            board = Board(nbRows: rows,nbCol: cols)!
        }
    }
    //test constructor
    public init(_ pl: [Player], printer: @escaping (String)->(),reader: @escaping (Bool)->String?){
        readerFunc = reader
        printerFunc = printer
        players = pl
        board = Board()
        winRules = [AreCols4xL(), AreRows4xL(), AreDiags4xL()]
        while(true){
            while(doGameLoop()){}
            printerFunc(winner == nil ? "stalemate!" : "player: "+String(winner?.id ?? -1)+" has won the game")
            if(winner != nil){ break }
            sleep(1)
            board = Board()
        }
    }
    
    public convenience init(_ test: Int, printer: @escaping (String)->(), reader: @escaping (Bool)->String?){
        self.init([IA(id: 0, reader: reader), IA(id: 1,reader: reader)], printer: printer, reader: reader)
    }
    
    private func doGameLoop() -> Bool{
        for p in players{
            var played: Int  = p.play(board.nbRow-1)
            while(!board.insertPeice(id: p.id, row: played)){
                printerFunc("Incorrect choice, pick another:\n")
                played = p.play(board.nbRow-1)
            }
            printerFunc(board.description)
            for gRule in winRules {
                if(gRule.isRuleMet(grid: board.playspace, targetedRow: board.lastinsert, targetedCol: played, lastPlayedId: p.id)){
                    winner = p
                    printerFunc(board.coalesceWonBoard(winningAlignementGrid: gRule.results ?? [[0]]))
                    return false
                }
                if board.isFull() { //stalemate
                    return false
                }
            }
        }
        
        return true
    }
    
    private func CreatePlayers() -> Bool{
        var humans:Int = 0
        var AI:Int = 0
        printerFunc("Création des joueurs, combien d'humains au total?\n>")
        if let typed = readerFunc(true) {if let num = Int(typed) {humans = num}}
        
        printerFunc("Création des IA, combien d'IA au total?\n>")
        if let typed = readerFunc(true) {if let num = Int(typed){AI = num}}
        
        var ids: Int = 0
        
        if(humans > 0){
            let humanRange = 1...humans
            for index in humanRange{
                players.append(Human(id: ids, reader: readerFunc))
                ids+=1
            }
        }
        
        if(AI > 0){
            let AIRange = humans...((humans)+(AI-1))
            for index in AIRange{
                players.append(IA(id: ids, reader: readerFunc))
                ids+=1
            }
        }
        
        printerFunc(String(players.count) + " joueurs créés")
        printerFunc(players.description)
        return humans+AI > 0
    }
    
    private func createBoard(){
        printerFunc("Création du tableau, combien de colonnes?\n>")
        if let typed = readerFunc(true) {if let num = Int(typed) {cols = num}}
        printerFunc("\ncombien de lignes?\n>")
        if let typed = readerFunc(true) {if let num = Int(typed) {rows = num}}
    }
}
