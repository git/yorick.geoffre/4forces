//
//  Player.swift
//  4forces
//
//  Created by yorick geoffre on 17/01/2023.
//

import Foundation

public class Player : CustomStringConvertible{
public var description: String{return "Player " + String(id)}
    internal var reader: (Bool) -> String?
internal var id: Int
    public init(id: Int, reader: @escaping (Bool)-> String?) {
        self.id = id
        self.reader = reader
    }
    public func play(_ max: Int)-> Int{
        return -1
    }
}
