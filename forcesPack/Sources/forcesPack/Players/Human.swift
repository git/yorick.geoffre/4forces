//
//  Human.swift
//  4forces
//
//  Created by yorick geoffre on 17/01/2023.
//

import Foundation

public class Human: Player {
    public override var description: String{return "Human " + String(id)}
    
    public override func play(_ max: Int)-> Int{
        print("pick a row (0-"+String(max)+"):\n>")
        if let input = reader(true) {
            if let number = Int(input){
                return number
            }
        }
        return -1
    }
}
