//
//  IA.swift
//  4forces
//
//  Created by yorick geoffre on 17/01/2023.
//

import Foundation

public class IA: Player {
    
    
    
    public override var description: String{return "IA " + String(id)}
    
    public override func play(_ max: Int)-> Int{
        return Int.random(in: 0...max)
    }
}
