//
//  File.swift
//  
//
//  Created by yorick geoffre on 09/02/2023.
//

import Foundation

public class IAIncer: IA {
    var ctr: Int = 0
    
    public override var description: String{return "IA incer" + String(id)}
    
    public override func play(_ max: Int)-> Int{
        if (ctr < max){ ctr += 1 }else{ ctr = 0 }
        return ctr
    }
}
