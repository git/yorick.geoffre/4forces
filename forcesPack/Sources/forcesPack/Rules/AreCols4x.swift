//
//  File 2.swift
//  
//
//  Created by yorick geoffre on 24/01/2023.
//

import Foundation
public class AreCols4x: gridRule, Rule{  //TODO: use the targeted row/col for more precision
    public func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int) -> Bool {
        var i: Int = 0
        var old: Int? = lastPlayedId
        var rowid = 0
        var colid = 0
        for row in grid{
            for cell in row{
                if(cell == old && cell != nil){
                    i+=1
                }else{
                    i = 0
                }
                old = cell
                if(i >= 3){
                        return true
                }
                colid += 1
            }
            rowid+=1
            colid = 0
            i = 0
        }
        return false;
    }
}
