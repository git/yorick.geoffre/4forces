//
//  File.swift
//  
//
//  Created by yorick geoffre on 07/02/2023.
//

import Foundation
public class gridRule{
    public init(){}
    internal var resultsGrid : [[Int?]]? = nil
    
    public var results: [[Int?]]? {get{return resultsGrid}}
    
    internal func prepareGrid(_ grid: [[Int?]]){
        resultsGrid = Array(repeating: Array( repeating: nil, count: grid[0].count), count: grid.count)
    }
}
