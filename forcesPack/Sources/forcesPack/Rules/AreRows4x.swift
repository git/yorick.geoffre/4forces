//
//  File.swift
//  
//
//  Created by yorick geoffre on 24/01/2023.
//

import Foundation

public class AreRows4x: gridRule, Rule{//TODO: use the targeted row/col for more precision
    public override init(){}
    public func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int) -> Bool {
        var rowid = 0
        var colid = 0
        var i: [Int] = Array( repeating: 0, count: grid.count)
        var old: [Int?] = Array( repeating: lastPlayedId, count: grid.count)
        var idx: Int = 0
        for row in grid{
            for cell in row{
                if(cell == old[idx] && cell != nil){
                    i[idx]+=1
                }else{
                    i[idx] = 0
                }
                old[idx] = cell
                if(i[idx] >= 3){
                        return true
                }
                colid += 1
                idx += 1
            }
            rowid += 1
            colid = 0
            idx = 0
        }
        return false;
    }
}
