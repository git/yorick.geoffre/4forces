//
//  File.swift
//  
//
//  Created by yorick geoffre on 31/01/2023.
//

import Foundation

public class AreDiags4x: gridRule, Rule{  //TODO: use the targeted row/col for more precision

    public func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int) -> Bool {
        
        let colRange = 0...grid.count-1
        let rowRange = 0...grid[0].count-1
        
        var oldCell: Int? = lastPlayedId
        var sameCount: Int = 0
        
        for indexC in colRange{ //donne la colonne de départ en bas du tableau
            var col = indexC
            var row = 0
            while(colRange.contains(col) && rowRange.contains(row)){
                if(grid[col][row] == oldCell && oldCell != nil){
                    sameCount += 1
                    if(sameCount >= 3)
                    {
                        return true
                    }
                }else{
                    sameCount = 0
                }
                oldCell = grid[col][row]
                col -= 1    //go back
                row += 1    //go up                this works like so: /
            }
            oldCell = nil
            sameCount = 0   //when we're done with a diagonal, we reset the counters
        }
        
        for indexC in colRange.reversed(){ //same thing in the other direction
            var col = indexC
            var row = 0
            while(colRange.contains(col) && rowRange.contains(row)){
                if(grid[col][row] == oldCell && oldCell != nil){
                    sameCount += 1
                    if(sameCount >= 3)
                    {
                        return true
                    }
                }else{
                    sameCount = 0
                }
                oldCell = grid[col][row]
                col += 1    //go next
                row += 1    //go up                 this works like so: \
            }
            oldCell = nil
            sameCount = 0
        }
        return false;
    }
}
