//
//  Rule.swift
//  4forces
//
//  Created by yorick geoffre on 17/01/2023.
//

import Foundation

public protocol Rule : gridRule{
    func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int)-> Bool
}
