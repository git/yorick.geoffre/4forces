//
//  File.swift
//  
//
//  Created by yorick geoffre on 30/01/2023.
//

import Foundation

public protocol nbRowsRule{
    var maxRow: Int { get }
    var maxCol: Int { get }
}
