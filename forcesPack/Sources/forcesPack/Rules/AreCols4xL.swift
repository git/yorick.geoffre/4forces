//
//  File.swift
//  forcesPack
//
//  Created by yorick geoffre on 08/02/2023.
//

import Foundation

public class AreCols4xL: gridRule, Rule{
    public func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int) -> Bool {
        var old: Int? = lastPlayedId
        var count: Int = 0
        var index: Int = 0
        for cell in grid[targetedCol]{
            if(cell == old && old != nil){
                count += 1
            }else{
                count = 0
            }
            old = cell ?? old
            if count >= 4{
                prepareGrid(grid)
                for idx in index-3...index{
                    resultsGrid?[targetedCol][idx] = old
                }
                return true;
            }
            index += 1
        }
        return false;
    }
}
