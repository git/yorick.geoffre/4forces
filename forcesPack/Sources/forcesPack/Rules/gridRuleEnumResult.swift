//
//  File.swift
//  
//
//  Created by yorick geoffre on 07/02/2023.
//

import Foundation

public enum gridRuleResult{
    case unfinished
    case won
    case stalemate
}
