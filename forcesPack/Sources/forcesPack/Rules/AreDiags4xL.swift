//
//  File.swift
//  forcesPack
//
//  Created by yorick geoffre on 08/02/2023.
//

import Foundation

public class AreDiags4xL: gridRule, Rule{

    public func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int) -> Bool {
        var old: Int? = lastPlayedId
        var count : Int = 0
        let rowsize = grid[0].capacity-1
        
        for index in -3...3{    // comme /
            if((0...grid.count-1).contains(targetedRow + index) && (0...rowsize).contains(targetedCol + index)){
                if(old == grid[targetedRow+index][targetedCol+index] && old != nil){
                    count += 1
                }else{
                    count = 0
                }
                old = grid[targetedRow+index][targetedCol+index]
                if(count >= 4){
                    prepareGrid(grid)
                    for idx in index-3...index{
                        resultsGrid?[targetedRow+idx][targetedCol+idx] = old
                    }
                    return true;
                }
            }else{
                count = 0
            }
        }
        //---------------------------------------------------------------
        count = 0
        old = grid[targetedCol][targetedRow]
        for index in -3...3{    // comme \
            if((0...grid.count-1).contains(targetedRow - index) && (0...rowsize).contains(targetedCol + index)){
                if(old == grid[targetedRow-index][targetedCol+index] && old != nil){
                    count += 1
                }else{
                    count = 0
                }
                old = grid[targetedRow-index][targetedCol+index]
                if(count >= 4){
                    prepareGrid(grid)
                    for idx in index-3...index{
                        resultsGrid?[targetedRow-idx][targetedCol+idx] = old
                    }
                    return true;
                }
            }else{
                count = 0
            }
        }
        return false;
    }
}
