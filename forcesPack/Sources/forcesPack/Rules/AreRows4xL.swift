//
//  File.swift
//  
//
//  Created by yorick geoffre on 07/02/2023.
//

import Foundation

public class AreRows4xL: gridRule, Rule{
    public override init(){}
    public func isRuleMet(grid: [[Int?]], targetedRow: Int, targetedCol: Int, lastPlayedId: Int) -> Bool {
        var old: Int? = lastPlayedId
        var count: Int = 0
        let cap = grid.count
        for index in 0...cap-1{
            if(grid[index][targetedRow] == old && old != nil){
                count += 1
            }else{
                count = 0
            }
            old = grid[index][targetedRow] ?? old
            if count >= 4{
                prepareGrid(grid)
                for idx in index-3...index{
                    resultsGrid?[idx][targetedRow] = old
                }
                return true;
            }
        }
        return false;
    }
}
