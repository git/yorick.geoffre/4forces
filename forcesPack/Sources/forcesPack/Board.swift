//
//  Board.swift
//  4forces
//
//  Created by yorick geoffre on 17/01/2023.
//

import Foundation

public struct Board : CustomStringConvertible{
    public var description: String{
        return toString()
    }
    
    public var lastinsert: Int{
        return lastInsertIndex
    }
    
    private var lastInsertIndex: Int = 0;
    
    private var nbRows = 4
    private var nbColumn = 4
    private var grid: [[Int?]]
    
    public var nbRow: Int {get{return nbRows}}
    
    public var nbCols : Int {
        get{nbColumn}
    }
    
    public var playspace: [[Int?]] {get{grid}}
    
    public init(){
        grid = Array(repeating: Array( repeating: nil, count: nbRows), count: nbColumn)
    }
    
    public init?(nbRows: Int, nbCol: Int){
        guard (nbRows > 1 && nbCol > 1) else {
            return nil
        }
        self.nbColumn = nbRows
        self.nbRows = nbCol
        grid = Array(repeating: Array( repeating: nil, count: nbRows), count: nbCol)
    }
    
    public init?(input: [[Int?]]){
        guard input[0][0] != nil else {return nil}  //inutile
        grid = input
        nbRows = input.count
        nbColumn = input[0].count
    }
    
    private mutating func insertPeice(id: Int, row: Int, col: Int) -> Bool{
        print("row: " , row , "col: " , col)
        if(grid[row][col] != nil){
            print("insert not nil err")
            return false;
        }
        lastInsertIndex = col
        grid[row][col] = id
        //print(grid)
        return true;
    }

    public mutating func insertPeice(id: Int, row: Int) -> Bool{
        guard !isFull() && row >= 0 && row < grid.capacity else{ return false }
        
        var colToInsert: Int = nbColumn-1
        
//        print("jeton vide: ",colToInsert)
        
        for cellNum in 0...nbColumn-1{    //parcours de la grille du haut vers le bas
//            print("for in: ", cellNum)
            let nbC = nbColumn
            if(grid[row][(nbColumn-1)-cellNum] == nil){colToInsert = (nbColumn-1)-cellNum; break}
        }
        
        if(grid[row][colToInsert] != nil){
            print("erreur de jeton non nil, valeur: ", grid[row][colToInsert])
            return false;
        }
//        print("jeton ok: ",colToInsert)
        
        return insertPeice(id: id, row: row, col: colToInsert) || isFull();
    }
    
    public func isFull()-> Bool{
        var isfull: Bool = true;
        for intarray in grid{
            for cell in intarray{
                if(cell == nil){
                    isfull = false; break;
                    
                }
            }
        }
        return isfull
    }
    
    subscript(row: Int, col: Int) -> Int?{
        get{
            guard row >= 0 && col >= 0 && row < nbRows && col < nbColumn else {return -404}
            return grid[row][col]
        }
    }
    
    private func toString() -> String{
        var str: String = ""
        str += "╔"
        str += String(repeating: "═╦", count: (nbRows)-1) + "═╗\n"
        for collNum in 0...nbColumn-1{    //parcours de la grille du bas vers le haut
            for rowNum in 0...nbRows-1{
                str += String("║"+((grid[rowNum][collNum]  == nil) ? "▒" : String((grid[rowNum][collNum] ?? 0))))
            }
            str += String("║\n")
        }
        str += "╚"
        str += String(repeating: "═╩", count: (nbRows)-1) + "═╝\n"
        return str
    }
    
    public func coalesceWonBoard(winningAlignementGrid: [[Int?]]) -> String{
        var str: String = ""
        str += "╔"
        str += String(repeating: "═╦", count: (nbRows)-1) + "═╗\n"
        for collNum in 0...nbColumn-1{    //parcours de la grille du bas vers le haut
            for rowNum in 0...nbRows-1{
                str += "║"
                str += String(winningAlignementGrid[rowNum][collNum] != nil ? "@" : " ")
            }
            str += String("║\n")
        }
        str += "╚"
        str += String(repeating: "═╩", count: (nbRows)-1) + "═╝\n"
        return str
    }
}
